import {useState} from 'react';
import {Button, Form, Col, Row} from 'react-bootstrap';
import './App.css';
function App() {

    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [result, setResult] = useState(0);

      function add() {
        setResult(Number(num1) + Number(num2))
      }

      function subtract() {
        setResult(Number(num1) - Number(num2))
      }

      function multiply() {
        setResult(Number(num1) * Number(num2))
      }

      function divide() {
        setResult(Number(num1) / Number(num2))
      }

      function reset() {
        setNum1(0)
        setNum2(0)
        setResult(0)
      }

  return (

    <>
      <Row className="w-50 mx-auto">
        <Col xs={12}>
          <h1 className="text-center m-5">Calculator</h1>
          <h1 className="text-center m-5">{result}</h1>
        </Col>

        <Col xs={12} md={6}>
          <Form.Control type="number" value={num1} onChange={(event) => setNum1(event.target.value)}/>
        </Col>
        <Col xs={12} md={6}>
          <Form.Control type="number" value={num2} onChange={(event) => setNum2(event.target.value)}/>
        </Col>

        <Col xs={12} className="text-center my-5">
          <Button variant="primary" className="mx-1 p-2" size="lg" onClick={add}>Add</Button>
          <Button variant="primary" className="mx-1 p-2" size="lg" onClick={subtract}>Subtract</Button>
          <Button variant="primary" className="mx-1 p-2" size="lg" onClick={multiply}>Multiply</Button>
          <Button variant="primary" className="mx-1 p-2" size="lg" onClick={divide}>Divide</Button>
          <Button variant="primary" className="mx-1 p-2" size="lg" onClick={reset}>Reset</Button>
        </Col>
      </Row>
    </>



      
    );
}

export default App;
